var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
var daysShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

var round;
for (round = 1; round < 35; round++) {
  $(".tabs ul").append("<li round='" + round + "'><a>" + round + "</a></li>")
  $("#fixtures-tables").append("<table round='" + round + "' class='table fixtures-table is-hoverable is-fullwidth matchday-" + round + "'><tbody></tbody></table>")
};

var shownRound;

function fixturesResults() {
  var matches = allMatches.matches;
  $.each(matches, function() {
    var match = $(this)[0];
    var gameSchedule = new Date(match.utcDate);

    var gameMonth = gameSchedule.getMonth();
    var gameMonth = months[gameMonth];

    var gameDate = gameSchedule.getDate();

    var gameDay = gameSchedule.getDay();
    var gameDay = daysShort[gameDay];

    var gameHour = gameSchedule.getHours();
    var gameMinutes = gameSchedule.getMinutes();

    var currentRound = match.season.currentMatchday;

    if (shownRound !== currentRound) {
      shownRound = currentRound;
      $(".fixtures-table").hide();
      $(".fixtures-table.matchday-" + currentRound).show();
      $(".tabs li[round='" + currentRound + "']").addClass("is-active");
      $(".tabs li[round='" + currentRound + "'] a").text("Current");
    }

    var result = "";

    if ( match.score.winner == "HOME_TEAM" ) {
      var result = "home-win";
    }
    else if ( match.score.winner == "AWAY_TEAM" ) {
      var result = "away-win";
    } else if ( match.score.winner == "DRAW" ) {
      var result = "draw";
    }

    var homeGoals = match.score.fullTime.homeTeam;
    var awayGoals = match.score.fullTime.awayTeam;

    if (match.score.fullTime.homeTeam == null && match.score.fullTime.awayTeam == null) {
      var homeGoals = "vs";
      var awayGoals = "";
    }

    $("<tr class='" + result + "'><td class='match-date is-hidden-mobile'>" + gameDay + " " + gameDate + " " + gameMonth + "</td><td class='home-team'>" + match.homeTeam.name + "</td><td class='home-score'><span>" + homeGoals + "</span></td><td class='away-score'><span>" + awayGoals + "</span></td><td class='away-team' >" + match.awayTeam.name + "</td></tr>").appendTo("#fixtures-tables .matchday-" + match.matchday + " tbody");
  });
};

$(".tabs li").click(function() {
  $(".tabs li").removeClass("is-active");
  $(this).addClass("is-active");

  var clickedRound = $(this).attr("round");
  $(".fixtures-table").hide();
  $(".fixtures-table.matchday-" + clickedRound).show();
  console.log(clickedRound);
});

fixturesResults();