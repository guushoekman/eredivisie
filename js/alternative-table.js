// alternative table
function alternativeTable() {
  var teams = standings.standings[0].table;
  var mostPoints = teams[0].points;
  var fewestPoints = teams[17].points;
  for (var i = mostPoints; i >= fewestPoints; i--) {
    $("<tr id='" + i + "'><th class='position'></th><td class='points'>" + i + "</td><td class='teams'></td></tr>").appendTo(".table tbody");
  };
  $.each(teams, function() {
    var team = $(this)[0];
    var netGoals = "negative";
    var positiveGoals = "";
    if (team.goalDifference > 0) {
      netGoals = "positive"
      positiveGoals = "+"
    };
    if (team.goalDifference == 0) {
      netGoals = "equal"
    };
    $("#" + team.points + " .points").css("color", "black");
    $("<div class='team-box'>" + team.team.name + "<span class='goal-difference " + netGoals + "'>" + positiveGoals + team.goalDifference + "</span></div>").appendTo($("#" + team.points + " .teams"))
    $("#" + team.points + " .position").text(team.position);
  });
}