// compact table
function compactTable() {
  var teams = standings.standings[0].table;
  $.each(teams, function() {
    var team = $(this)[0];

    $("<tr><th>" + team.position + "</th><td class='team-name'>" + team.team.name + "</td><td class='team-points'>" + team.points + "</td><td class='team-difference'>" + team.goalDifference + "</td></tr>").appendTo(".compact-table tbody");
  });
};

// get current round
var currentRound = standings.season.currentMatchday;
var lastRound = currentRound - 1;

// latest results
function latestResults() {
  var matches = allMatches.matches;
  $.each(matches, function() {
    var match = $(this)[0];

    var result = "";

    if ( match.score.winner == "HOME_TEAM" ) {
      var result = "home-win";
    }
    else if ( match.score.winner == "AWAY_TEAM" ) {
      var result = "away-win";
    } else if ( match.score.winner == "DRAW" ) {
      var result = "draw";
    }

    var homeGoals = match.score.fullTime.homeTeam;
    var awayGoals = match.score.fullTime.awayTeam;

    if (match.score.fullTime.homeTeam == null && match.score.fullTime.awayTeam == null) {
      var homeGoals = "vs";
      var awayGoals = "";
    }

    $("<tr class='round-" + match.matchday + " " + result + "'><td class='home-team'>" + match.homeTeam.name + "</td><td class='home-score'><span>" + homeGoals + "</span></td><td class='away-score'><span>" + awayGoals + "</span></td><td class='away-team' >" + match.awayTeam.name + "</td></tr>").appendTo(".latest-results tbody");
    $(".latest-results .round-" + lastRound).show();
  });
};

// upcoming round
function upcomingFixtures() {
  var matches = allMatches.matches;
  $.each(matches, function() {
    var match = $(this)[0];

    var result = "";

    $("<tr class='round-" + match.matchday + "'><td class='home-team'>" + match.homeTeam.name + "</td><td class='vs'>vs</td><td class='away-team' >" + match.awayTeam.name + "</td></tr>").appendTo(".upcoming-fixtures tbody");
    $(".upcoming-fixtures .round-" + currentRound).show();
  });
};

compactTable();
latestResults();
upcomingFixtures();

// make width of column on both charts equal
var homeWidth = $(".latest-results .round-" + lastRound + " .home-team").width();
var awayWidth = $(".latest-results .round-" + lastRound + " .away-team").width();
var vsWidth = $(".latest-results .round-" + lastRound + " .home-score").width() * 2;

$(".upcoming-fixtures .round-" + currentRound + " td.vs").width(vsWidth);
$(".upcoming-fixtures .round-" + currentRound + " td.home-team").width(homeWidth);
$(".upcoming-fixtures .round-" + currentRound + " td.away-team").width(awayWidth);

// remove line below last visible match
$(".latest-results .round-" + lastRound).last().addClass("last-shown");
$(".upcoming-fixtures .round-" + currentRound).last().addClass("last-shown");
