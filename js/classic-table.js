// standard table
function classicTable() {
  var teams = standings.standings[0].table;
  var mostWon = 0;
  var leastWon = 1000;

  var mostLost = 0;
  var leastLost = 1000;

  var mostScored = 0;
  var leastScored = 1000;

  var mostConceded = 0;
  var leastConceded = 1000;

  var bestDifference = 0;
  var worstDifference = 0;
  $.each(teams, function() {
    var team = $(this)[0];

    // won
    if (team.won > mostWon) {
      mostWon = team.won
    };
    if (team.won < leastWon) {
      leastWon = team.won
    };

    // lost
    if (team.lost > mostLost) {
      mostLost = team.lost
    };
    if (team.lost < leastLost) {
      leastLost = team.lost
    };

    // scored
    if (team.goalsFor > mostScored) {
      mostScored = team.goalsFor
    };
    if (team.goalsFor < leastScored) {
      leastScored = team.goalsFor
    };

    // conceded
    if (team.goalsAgainst > mostConceded) {
      mostConceded = team.goalsAgainst
    };
    if (team.goalsAgainst < leastConceded) {
      leastConceded = team.goalsAgainst
    };

    // goal difference
    if (team.goalDifference > bestDifference) {
      bestDifference = team.goalDifference
    };
    if (team.goalDifference < worstDifference) {
      worstDifference = team.goalDifference
    };

    $("<tr><th>" + team.position + "</th><td class='team-name'>" + team.team.name + "</td><td class='team-points'>" + team.points + "</td><td class='team-played'>" + team.playedGames + "</td><td class='team-won'>" + team.won + "</td><td class='team-draw'>" + team.draw + "</td><td class='team-lost'>" + team.lost + "</td><td class='team-goals'>" + team.goalsFor + "</td><td class='team-against'>" + team.goalsAgainst + "</td><td class='team-difference'>" + team.goalDifference + "</td></tr>").appendTo(".classic-table tbody");
  });
  $(".classic-table .team-won").each(function() {
    var teamWon = $(this).text();
    if (teamWon == mostWon) {
      $(this).addClass("best");
    }
    if (teamWon == leastWon) {
      $(this).addClass("worst");
    }
  });

  $(".classic-table .team-lost").each(function() {
    var teamLost = $(this).text();
    if (teamLost == mostLost) {
      $(this).addClass("worst");
    }
    if (teamLost == leastLost) {
      $(this).addClass("best");
    }
  });

  $(".classic-table .team-goals").each(function() {
    var goalsFor = $(this).text();
    if (goalsFor == mostScored) {
      $(this).addClass("best");
    }
    if (goalsFor == leastScored) {
      $(this).addClass("worst");
    }
  });

  $(".classic-table .team-against").each(function() {
    var goalsAgainst = $(this).text();
    if (goalsAgainst == mostConceded) {
      $(this).addClass("worst");
    }
    if (goalsAgainst == leastConceded) {
      $(this).addClass("best");
    }
  });

  $(".classic-table .team-difference").each(function() {
    var goalDifference = $(this).text();
    if (goalDifference == bestDifference) {
      $(this).addClass("best");
    }
    if (goalDifference == worstDifference) {
      $(this).addClass("worst");
    }
  });
}

classicTable();