# Eredivisie.in

A collection of data visualisations related to the Eredivisie or Dutch football in general

## Sources of data

### [football-data.org](http://api.football-data.org/)

#### Standings

`curl -s -H "X-Auth-Token: {api-key}" https://api.football-data.org/v2/competitions/2003/standings > eredivisie/js/standings.json && awk '{print "var standings = " $0}' eredivisie/js/standings.json > eredivisie/js/standings.js`

#### Competition

`curl -s -H "X-Auth-Token: {api-key}" https://api.football-data.org/v2/competitions/2003 > eredivisie/js/competition.json && awk '{print "var competition = " $0}' eredivisie/js/competition.json > eredivisie/js/competition.js`

#### Matches

`curl -s -H "X-Auth-Token: {api-key}" https://api.football-data.org/v2/competitions/2003/matches > eredivisie/js/matches.json && awk '{print "var matches = " $0}' eredivisie/js/matches.json > eredivisie/js/matches.js`

